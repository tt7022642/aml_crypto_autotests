@REM Создание виртуального окружения
python -m venv venv

@REM Активация виртуального окружения
.\venv\Scripts\Activate.ps1

@REM Установка зависимостей
pip install -r requirements.txt

@REM Запуск тестов
python -m pytest

@REM Открытие отчета в браузере
allure serve ./allure-results