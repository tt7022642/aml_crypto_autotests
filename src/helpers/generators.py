import random
import string


def generate_string_of_digits(length: int):
    """
    Генерирует строку из случайных цифр заданной длины.

    :param length: Длина строки (количество символов).
    """
    return "".join(random.choice(string.digits[1:]) for _ in range(length))
