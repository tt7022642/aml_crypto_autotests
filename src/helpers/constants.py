from enum import StrEnum


class URL(StrEnum):
    BaseUrl = "https://www.globalsqa.com/angularJs-protractor/BankingProject/#/login"


class LoginType(StrEnum):
    customer = "customer"
    manager = "manager"


class CustomerAccountSections(StrEnum):
    transactions = "transactions"
    deposit = "deposit"
    withdrawal = "withdrawal"


class ManagerAccountSections(StrEnum):
    addCustomer = "addCustomer"
    openAccount = "openAccount"
    customers = "customers"
