"""
В этом модуле описываются сценарии входа в приложение
"""

import pytest
from src.helpers.constants import LoginType


@pytest.mark.usefixtures("main_login_page", "customer_login_page")
def auth_by_credentials(
    login_type,
    main_login_page,
    customer_login_page=None,
    from_current_page: bool = False,
):
    if from_current_page:
        main_login_page.click_home_button()
    main_login_page.select_login_type(login_type)
    if login_type == LoginType.customer:
        customer_login_page.select_customer_name("Neville Longbottom")
        customer_login_page.click_login_button()
