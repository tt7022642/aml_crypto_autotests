"""
В этом модуле описаны методы для работы с элементами на странице аккаунта пользователя.
"""
from allure import step
from selenium.webdriver.common.by import By

from src.helpers.constants import CustomerAccountSections
from src.pages.base_page import BasePageLocators, BasePage


class CustomerAccountPageLocators(BasePageLocators):
    TRANSACTIONS_SECTION = (
        By.XPATH,
        "//button[contains(@class, 'tab') and contains(text(), 'Transactions')]",
    )
    DEPOSIT_SECTION = (
        By.XPATH,
        "//button[contains(@class, 'tab') and contains(text(), 'Deposit')]",
    )
    WITHDRAWAL_SECTION = (
        By.XPATH,
        "//button[contains(@class, 'tab') and contains(text(), 'Withdrawl')]",
    )

    WITHDRAWAL_FIELD = (By.XPATH, "//input[@type='number']")
    WITHDRAW_BTN = (
        By.XPATH,
        "//button[@class='btn btn-default' and text()='Withdraw']",
    )
    WITHDRAW_ERROR_MESSAGE = (
        By.XPATH,
        "//span[contains(@class, 'error') and contains(@class, 'ng-binding')]",
    )


class CustomerAccountPage(BasePage):
    loc = CustomerAccountPageLocators()
    section_locators = {
        "transactions": loc.TRANSACTIONS_SECTION,
        "deposit": loc.DEPOSIT_SECTION,
        "withdrawal": loc.WITHDRAWAL_SECTION,
    }

    withdrawal_field = loc.WITHDRAWAL_FIELD
    withdraw_button = loc.WITHDRAW_BTN
    withdraw_error_message = loc.WITHDRAW_ERROR_MESSAGE

    def select_customer_account_section(self, section_title: CustomerAccountSections):
        """
        Выбирает указанную секцию в меню на странице аккаунта клиента.

        :param section_title: Название секции. Допустимые значения: helpers.constants.CustomerAccountSections.
        """
        with step(f"Выбираем секцию в меню клиента: {section_title}"):
            if section_title in self.section_locators:
                self.click_element(self.section_locators[section_title])
            else:
                raise ValueError(
                    "Недопустимая секция меню. Допустимые значения: 'transactions', 'deposit', 'withdrawal'"
                )

    def enter_withdrawal_amount(self, value):
        """
        Вводит сумму для снятия со счета.
        """
        self.send_value(self.withdrawal_field, value)

    def click_withdraw_button(self):
        self.click_element(self.withdraw_button)

    def get_error_message(self):
        return self.find_element(self.withdraw_error_message)
