"""
В этом модуле описаны методы которые могут быть использованы на разных экранах
"""

from src.core import Element
from selenium.webdriver.common.by import By


class BasePageLocators:
    HOME_BTN = (By.XPATH, "//button[@class='btn home' and text()='Home']")


class BasePage(Element):
    loc = BasePageLocators()
    home_button = loc.HOME_BTN

    def click_home_button(self):
        self.click_element(self.home_button)
