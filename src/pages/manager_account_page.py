"""
В этом модуле описаны методы для работы с элементами на странице менеджера
"""

from allure import step
from selenium.webdriver.common.by import By

from src.helpers.constants import ManagerAccountSections
from src.pages.base_page import BasePage, BasePageLocators


class ManagerAccountPageLocators(BasePageLocators):
    ADD_CUSTOMER_SECTION = (
        By.XPATH,
        "//button[@class='btn btn-lg tab' and contains(text(), 'Add Customer')]",
    )
    OPEN_ACCOUNT_SECTION = (
        By.XPATH,
        "//button[@class='btn btn-lg tab' and contains(text(), 'Open Account')]",
    )
    CUSTOMERS_SECTION = (
        By.XPATH,
        "//button[@class='btn btn-lg tab' and contains(text(), 'Customers')]",
    )

    FIRST_NAME_FIELD = (By.XPATH, "//input[@placeholder='First Name']")
    LAST_NAME_FIELD = (By.XPATH, "//input[@placeholder='Last Name']")
    POST_CODE_FIELD = (By.XPATH, "//input[@placeholder='Post Code']")

    ADD_CUSTOMER_BTN = (By.XPATH, "//button[text()='Add Customer']")


class ManagerAccountPage(BasePage):
    loc = ManagerAccountPageLocators()
    section_locators = {
        "addCustomer": loc.ADD_CUSTOMER_SECTION,
        "openAccount": loc.OPEN_ACCOUNT_SECTION,
        "customers": loc.CUSTOMERS_SECTION,
    }

    first_name_field = loc.FIRST_NAME_FIELD
    last_name_field = loc.LAST_NAME_FIELD
    post_code_field = loc.POST_CODE_FIELD

    add_customer_button = loc.ADD_CUSTOMER_BTN

    def select_manager_account_section(self, section_title: ManagerAccountSections):
        """
        Выбирает секцию меню на странице банковского менеджера.

        :param section_title: Допустимые значения: helpers.constants.ManagerAccountSections.
        """
        with step(f"Выбираем секцию в меню менеджера: {section_title}"):
            if section_title in self.section_locators:
                self.click_element(self.section_locators[section_title])
            else:
                raise ValueError(
                    "Недопустимая секция меню. Допустимые значения: 'addCustomer', 'openAccount', 'customers'"
                )

    def enter_customer_data(self, first_name, last_name, post_code):
        """
        Вводит данные нового клиента в соответствующие поля формы.
        :param first_name: Имя нового клиента.
        :param last_name: Фамилия нового клиента.
        :param post_code: Почтовый индекс нового клиента.
        """
        with step("Вводим имя"):
            self.send_value(self.first_name_field, first_name)
        with step("Вводим фамилию"):
            self.send_value(self.last_name_field, last_name)
        with step("Вводим почтовый индекс"):
            self.send_value(self.post_code_field, post_code)

    def click_add_customers_button(self):
        self.click_element(self.add_customer_button)
