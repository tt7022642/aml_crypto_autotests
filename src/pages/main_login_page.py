"""
В этом модуле описаны методы для работы с элементами на странице входа.
"""
from allure import step
from selenium.webdriver.common.by import By

from src.helpers.constants import LoginType
from src.pages.base_page import BasePage, BasePageLocators


class LoginPageLocators(BasePageLocators):
    CUSTOMER_LOGIN_BTN = (
        By.XPATH,
        "//button[@class='btn btn-primary btn-lg' and contains(text(), 'Customer Login')]",
    )
    BANK_MANAGER_LOGIN_BTN = (
        By.XPATH,
        "//button[@class='btn btn-primary btn-lg' and contains(text(), 'Bank Manager Login')]",
    )


class MainLoginPage(BasePage):
    loc = LoginPageLocators()
    login_type_locators = {
        "customer": loc.CUSTOMER_LOGIN_BTN,
        "manager": loc.BANK_MANAGER_LOGIN_BTN,
    }

    def select_login_type(self, login_type: LoginType):
        """
        Выбирает тип входа на странице в зависимости от переданного параметра.

        :param login_type: Тип входа. Допустимые значения: helpers.constants.LoginType
        """
        with step(f"Выбираем под кем будем заходить - {login_type}"):
            if login_type in self.login_type_locators:
                self.click_element(self.login_type_locators[login_type])
            else:
                raise ValueError(
                    "Недопустимый тип логина. Допустимые значения: 'customer' или 'manager'"
                )
