"""
В этом модуле описаны методы для работы с элементами на странице входа пользователя.
"""

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

from src.pages.base_page import BasePageLocators, BasePage


class CustomerLoginPageLocators(BasePageLocators):
    DROPDOWN_LIST = (By.XPATH, "//select[@id='userSelect']")
    LOGIN_BTN = (By.XPATH, "//button[@class='btn btn-default' and @type='submit']")


class CustomerLoginPage(BasePage):
    loc = CustomerLoginPageLocators()
    dropdown_list = loc.DROPDOWN_LIST
    login_button = loc.LOGIN_BTN

    def select_customer_name(self, user_name):
        """
        Выбирает указанное имя клиента из выпадающего списка на странице входа клиента.

        :param user_name: Имя клиента, которое необходимо выбрать из выпадающего списка.
        """
        dropdown = self.find_element(self.dropdown_list)
        select = Select(dropdown)
        select.select_by_visible_text(user_name)

    def click_login_button(self):
        self.click_element(self.login_button)
