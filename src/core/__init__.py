"""
В этом модуле описаны общие методы для работы с веб элементами и окном браузера
"""

from allure import step
from selenium.common import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


class Element:
    def __init__(self, driver):
        self.driver = driver
        self.default_wait_time = 10

    def find_element(self, element_locator, wait_time=None):
        """
        Ищет элемент, используя указанный локатор, со временем ожидания.
        :param element_locator: Локатор элемента.
        :param wait_time: Время ожидания в секундах, по умолчанию 10 сек.
        :return: Найденный элемент.
        """
        if wait_time is None:
            wait_time = self.default_wait_time
        with step(f"Ищем элемент по локатору - {element_locator}"):
            try:
                wait = WebDriverWait(self.driver, wait_time)
                return wait.until(EC.visibility_of_element_located(element_locator))
            except TimeoutException:
                raise TimeoutException(
                    f"Элемент с локатором - {element_locator} не был найден за {wait_time} seconds."
                )

    def find_alert(self, wait_time=None):
        """
        Ищет всплывающее окно (alert) на текущей странице и возвращает его объект.
        """
        if wait_time is None:
            wait_time = self.default_wait_time
        with step("Ищем всплывающее окно (alert) на текущей странице"):
            try:
                wait = WebDriverWait(self.driver, wait_time)
                return wait.until(EC.alert_is_present())
            except TimeoutException:
                raise TimeoutException(
                    "Алерт не был найден в течение установленного времени"
                )

    def click_element(self, element_locator, wait_time=None):
        """
        Нажимает кнопку, определенную указанным локатором, с необязательным временем ожидания.

        :param element_locator: Локатор кнопки.
        :param wait_time: Время ожидания в секундах, по умолчанию 10 сек.
        """
        with step(f"Кликаем по элементу с локатором - {element_locator}"):
            button = self.find_element(element_locator, wait_time)
            button.click()

    def send_value(self, field_locator, to_send):
        """
        Ищет поле по локатору и заполняет его
        :param field_locator: Локатор поля для заполниения
        :param to_send: Значение, которое нужно ввести в поле.
        """
        with step(f"Вводим в поле текст со значением - {to_send}"):
            field = self.find_element(field_locator)
            field.send_keys(to_send)


class Window:
    def just_scroll(self):
        raise NotImplemented

    def get_back(self):
        raise NotImplemented
