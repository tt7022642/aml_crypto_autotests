import pytest
from faker import Faker
import allure

fake = Faker()


@pytest.fixture(scope="function")
@allure.title("Генерация данных пользователя")
def fake_customer_data():
    """
    Генерирует фиктивные данные для клиента.

    :return: Словарь с фиктивными данными:
             - "first_name": имя клиента,
             - "last_name": фамилия клиента,
             - "post_code": почтовый индекс.
    """
    first_name = fake.first_name()
    last_name = fake.last_name()
    post_code = fake.postcode()

    return {"first_name": first_name, "last_name": last_name, "post_code": post_code}
