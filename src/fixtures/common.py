import allure
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from src.helpers.constants import URL


@pytest.fixture(scope="function")
@allure.title("Инициализация драйвера")
def driver():
    options = Options()
    options.add_argument("--headless")
    driver = webdriver.Chrome(
        service=ChromeService(ChromeDriverManager().install()), options=options
    )
    driver.get(URL.BaseUrl)

    yield driver
