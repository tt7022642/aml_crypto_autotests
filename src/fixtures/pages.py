import pytest

from src.pages.customer_account_page import CustomerAccountPage
from src.pages.customer_login_page import CustomerLoginPage
from src.pages.main_login_page import MainLoginPage
from src.pages.manager_account_page import ManagerAccountPage


@pytest.fixture(scope="function")
def main_login_page(driver):
    return MainLoginPage(driver)


@pytest.fixture(scope="function")
def customer_login_page(driver):
    return CustomerLoginPage(driver)


@pytest.fixture(scope="function")
def customer_account_page(driver):
    return CustomerAccountPage(driver)


@pytest.fixture(scope="function")
def manager_account_page(driver):
    return ManagerAccountPage(driver)
