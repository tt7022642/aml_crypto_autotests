#!/bin/bash
# Создание виртуального окружения
python3 -m venv venv

# Активация виртуального окружения
source venv/bin/activate

# Установка зависимостей
pip install -r requirements.txt

# Запуск тестов
python -m pytest

# Открытие отчета в браузере
allure serve ./allure-results