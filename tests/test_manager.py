"""
В этом модуле описаны тесты для сценариев менеджера
"""

import pytest
from allure import title, step, attach, attachment_type

from src.helpers.constants import LoginType, ManagerAccountSections
from src.scenarios import auth


@pytest.fixture(scope="function", autouse=True)
@title("Авторизация под менеджером")
def env_setup(main_login_page):
    auth.auth_by_credentials(LoginType.manager, main_login_page)


@title("Проверка корректного создания клиента")
def test_create_customer(manager_account_page, fake_customer_data):
    with step("Создание клиента"):
        manager_account_page.select_manager_account_section(
            ManagerAccountSections.addCustomer
        )
        manager_account_page.enter_customer_data(
            fake_customer_data["first_name"],
            fake_customer_data["last_name"],
            fake_customer_data["post_code"],
        )
        manager_account_page.click_add_customers_button()

    with step("Проверка результата операции"):
        alert_text = manager_account_page.find_alert().text
        expected_alert_text = "Customer added successfully"

        assert (
            expected_alert_text in alert_text
        ), f"Ожидаемый текст '{expected_alert_text}' не найден в фактическом сообщении: '{alert_text}'"

        attach(
            str(fake_customer_data),
            name="Данные созданного клиента",
            attachment_type=attachment_type.TEXT,
        )
