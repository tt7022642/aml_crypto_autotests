"""
В этом модуле описаны тесты для сценариев пользователя
"""

import pytest
from allure import step, title, attach, attachment_type
from src.helpers.constants import LoginType, CustomerAccountSections
from src.helpers.generators import generate_string_of_digits
from src.scenarios import auth


@pytest.fixture(scope="function", autouse=True)
@title("Авторизация под клиентом")
def env_setup(main_login_page, customer_login_page):
    auth.auth_by_credentials(LoginType.customer, main_login_page, customer_login_page)


@title("Проверка снятия средств при нулевом балансе")
def test_withdrawing_funds_with_zero_balance(customer_account_page):
    with step("Попытка снятия средств со счета"):
        customer_account_page.select_customer_account_section(
            CustomerAccountSections.withdrawal
        )
        withdrawal_amount = generate_string_of_digits(4)
        customer_account_page.enter_withdrawal_amount(withdrawal_amount)
        customer_account_page.click_withdraw_button()

        error_message = customer_account_page.get_error_message().text
        expected_error_message = "Transaction Failed"

        assert (
            expected_error_message in error_message
        ), f"Ожидаемый текст '{expected_error_message}' не найден в фактическом сообщении об ошибке: '{error_message}'"

        attach(
            withdrawal_amount,
            name="Запрошенная сумма снятия",
            attachment_type=attachment_type.TEXT,
        )

        attach(
            error_message,
            name="Сообщение об ошибке",
            attachment_type=attachment_type.TEXT,
        )
